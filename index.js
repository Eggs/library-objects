"use strict";

class Book {
    constructor (title, author, pages, read) {
        this.title = title;
        this.author = author;
        this.pages = pages;
        this.read = read;
    }
    info() {
        return `${this.title} by ${this.author}, ${this.pages} pages not read yet.`;
    }
}

class Library {
    library = [];

    addBookToLibrary(book) {
        this.library.push(book);
    }

    changeReadStatus(bookIndex) {
        this.library[bookIndex].read = !this.library[bookIndex].read;
    }
    render() {
        const theLibrary = document.querySelector('.library');
        theLibrary.innerHTML = "";
    
        this.library.forEach(book => {
            const bookIndex = this.library.indexOf(book);
    
            const newBook = document.createElement('TR');
            newBook.innerHTML = `<td id='book-info' class="
                ${this.library[bookIndex].read ? 'checked' : ''}">` 
                + book.info() + "</td>" 
                + "<td class='remove-book'>X</td>";
            newBook.setAttribute('data-index-number', bookIndex);
    
            theLibrary.appendChild(newBook);
    
            // if (library[bookIndex].read) {
            //     document.getElementById('book-info').classList.add('checked');
            // }
        });
        addRemoveBookButtonsEventListener();
        addChangeReadStatusEventListener();
    }
}

const theHobbit = new Book('The Hobbit', 'J R R Tolkin', '295', true);
const stress = new Book('The Stress Solution', 'Dr Rangan', '300', true);

// Event listener functions
function addNewBookFormEventListener() {
    const formButton = document.querySelector('#book-submit');
    formButton.addEventListener('click', e => {
        const bookTitle = document.querySelector('#book-title').value;
        const bookAuthor = document.querySelector('#book-author').value;
        const bookPages = document.querySelector('#book-pages').value;
        const bookRead = document.querySelector('#book-read');
        const bookReadValue = bookRead.options[bookRead.selectedIndex].value === "true" ? true : false;
        console.log(bookRead.options);

        const newBook = new Book(bookTitle, bookAuthor, bookPages, bookReadValue);
        library.addBookToLibrary(newBook);
        library.render();
        e.preventDefault();

    });
}

function addShowNewBookDialogEventListener() {
    const showNewBookDialog = document.querySelector('#show-form');
    const newBookForm = document.querySelector('#new-book-form');
    showNewBookDialog.addEventListener('click', e => {
        if (newBookForm.style.display === 'none') {
            newBookForm.style.display = 'flex';
        } else {
            newBookForm.style.display = 'none'
        }
    });

}

function addRemoveBookButtonsEventListener() {
    const removeBookButtons = Array.from(document.getElementsByClassName('remove-book'));
    removeBookButtons.forEach(button => {
        button.addEventListener('click', (e) => {
            let libraryIndex = button.parentElement.getAttribute('data-index-number');
            library.splice(libraryIndex, 1);
            render();
        });
    });
}

function addChangeReadStatusEventListener() {
    const checkbox = document.querySelectorAll('.library td');
    checkbox.forEach(tableRow => {
    tableRow.addEventListener('click', (ev) => {
        if (ev.target.tagName === 'TD') {
            ev.target.classList.toggle('checked');
            library.changeReadStatus(ev.target.parentElement.getAttribute('data-index-number'));
        }
    });
});
}
 
//Init
const library = new Library();

library.addBookToLibrary(theHobbit);
library.addBookToLibrary(stress);

library.render();

addNewBookFormEventListener();
addShowNewBookDialogEventListener();
